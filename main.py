import os
import urllib
import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class Main(webapp2.RequestHandler):
    def get(self):
        params={}
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(params))

class Contacts(webapp2.RequestHandler):
    def get(self):
        params={}
        template = JINJA_ENVIRONMENT.get_template('contacts.html')
        self.response.write(template.render(params))

class QR_Code(webapp2.RequestHandler):
    def get(self):
        params={}
        template = JINJA_ENVIRONMENT.get_template('imageserver.html')
        self.response.write(template.render(params))
    def post(self):
        chs = self.request.get('chs')
        cht = self.request.get('cht')
        chld = 'H|1'
        chl = cgi.escape(self.request.get('chl'),quote=True)
        qr_url='http://chart.apis.google.com/chart?chs=%s&cht=%s&chld=%s&chl=%s'%(chs,cht,chld,chl)
        qr = '<img src="%s" width="300" height="300" alt="" />'%qr_url
        params={'qr':qr}
        template = JINJA_ENVIRONMENT.get_template('imageserver.html')
        self.response.write(template.render(params))

class NotFound(webapp2.RequestHandler):
    def get(self):
        params={}
        template = JINJA_ENVIRONMENT.get_template('404.html')
        self.response.write(template.render(params))


app = webapp2.WSGIApplication([('/', Main),
                              ('/qr_code', QR_Code),
                              ('/contacts', Contacts),
                              (r'/.*', NotFound)
                              ],
                              debug=True)

def main():
    run_wsgi_app(app)

if __name__ == "__main__":
    main()